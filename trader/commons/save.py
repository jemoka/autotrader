import tensorflow as tf
from datetime import datetime
from ..Agent import BuyerAgent
from ..Agent import BidderAgent
from ..Agent import CombinedAgent
from ..Enviroment import PoloniexEnv
from ..Policy import DeepQLearningPolicy 
from ..Policy import DeepRegressionPolicy
from ..Policy import CombinedAgentPolicy

import uuid
import time
import pickle
import os
import pyperclip

def SaveTrader(directory, enviroment, policy, id):
    print("Saving Agent...")
    policyData = policy.export(os.path.join(directory, "data", id), id)
    print("Generated Policy CKPT Tensorflow Data:", policyData[0])
    policyDir = os.path.join(os.path.join(directory, "data", id), id+"_POLICY.pkl")
    output = open(policyDir, 'wb+')
    pickle.dump(policyData[1], output)
    print("Generated Policy PKL Pickle Data:", policyDir)
    output.close()

    envData = enviroment.export()
    envDir = os.path.join(os.path.join(directory, "data", id), id+"_ENV.pkl")
    output = open(envDir, 'wb+')
    pickle.dump(envData, output)
    print("Generated Enviroment PKL Pickle Data:", envDir)
    output.close()
    pyperclip.copy(id)
    print("Saved! The ID of the Agent is ",id+" and is copied to clipboard.")

def OpenTrader(directory, id, atype):
    print("Restoring Agent...")

    policyDir = os.path.join(os.path.join(directory, "data", id), id+"_POLICY.ckpt")
    open_first = open(os.path.join(os.path.join(directory, "data", id), id+"_POLICY.pkl"), "rb")
    data = pickle.load(open_first)

    epilson = float(input("Unceartainty: "))

    if atype == "b":
        policy = DeepQLearningPolicy(restoreObject=[policyDir, data])
    elif atype == "i":
        policy = DeepRegressionPolicy(restoreObject=[policyDir, data])
    else:
        policy = CombinedAgentPolicy(epilson=epilson, restoreObject=[policyDir, data])

    open_first.close()

    open_first = open(os.path.join(os.path.join(directory, "data", id), id+"_ENV.pkl"), "rb")
    data = pickle.load(open_first)
    env = PoloniexEnv(dataObject=data)

    open_first.close()

    if atype == "b":
        agent = BuyerAgent(policy, env)
    elif atype == "i":
        agent = BidderAgent(policy, env)
    else:
        agent = CombinedAgent(policy, env)

    print("Restored! The ID of the Agent is ",id+". The Directory is ", directory)

    return agent, env, policy


    
