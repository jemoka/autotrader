import os
import urllib
import json
import random
import requests
import numpy as np
from .commons.encode import Autoencoder

class PoloniexEnv(object):
    def __init__(self, startingFund=10000.0, startingBTCAmount=1.0, rewardMultiplier=1, idleReward=-0.004, dataObject=None):
        
        if dataObject:
            self.fund = dataObject['fund']
            self.coin = dataObject['coin']
            self.initFund = dataObject['initFund']
            self.initCoin = dataObject['initCoin']
            self.multiplier = dataObject['multiplier']
            self.idleReward = dataObject['idleReward']
            self.currentOrderBook = dataObject['currentOrderBook']
            self.tradeIndex = dataObject['tradeIndex']
            self.orderState = False

            self.encoder = Autoencoder(4, 1, learningRate=0.01)
            self.encoder.train(self.getOrderBook(), epoch=10000)

            return
        self.fund = startingFund
        self.coin = startingBTCAmount
        
        self.initFund = self.fund
        self.initCoin = self.coin
        
        self.multiplier = rewardMultiplier
        self.idleReward = idleReward
        
        self.currentOrderBook = None
        self.tradeIndex = 0
        self.orderState = False
        
        self.encoder = Autoencoder(4, 1, learningRate=0.01)
        self.encoder.train(self.getOrderBook(), epoch=10000)

    def export(self):
        return {
            "fund": self.fund,
            "coin": self.coin,
            "initFund": self.initFund,
            "initCoin": self.initCoin,
            "multiplier": self.multiplier,
            "idleReward": self.idleReward,
            "currentOrderBook": self.currentOrderBook,
            "tradeIndex": self.tradeIndex,
        }
        
    def getOrderBook(self):
        try:
            with urllib.request.urlopen('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC') as response:
                orderBook = json.load(response)
        except:
            return self.getOrderBook()
            
        for i in orderBook['asks']:
            i[0] = float(i[0])
            # i.append(60)
            # i.append(40)
            i.append(1)
            i.append(1)
            
        for i in orderBook['bids']:
            i[0] = float(i[0]) 
            # i.append(40)
            # i.append(60)
            i.append(1)
            i.append(1)
            
            
        self.currentOrderBook = orderBook['asks']+orderBook['bids'] 
        
        if len(self.currentOrderBook) <= 0:
            return self.getOrderBook()
        
        try:
            self.currentOrderBook[0]
        except IndexError:
            return self.getOrderBook()
        
        return self.currentOrderBook
    
    def getInitState(self):
        return self.getLiveData()
        
    def resetEnv(self):
        self.fund = self.initFund
        self.coin = self.initCoin
    
    def getPortfolioCaptal(self, marketPrice):
        return self.fund+self.coin*marketPrice
    
    def getMarketPriceAverage(self):
        try:
            with urllib.request.urlopen('https://poloniex.com/public?command=returnTicker') as response:
                tickerData = json.load(response)["USDT_BTC"]
        except:
            return self.getMarketPriceAverage()
            
        return (float(tickerData["lowestAsk"])+float(tickerData["highestBid"]))/2 

    def getFullTicker(self):
        try:
            with urllib.request.urlopen('https://poloniex.com/public?command=returnTicker') as response:
                tickerData = json.load(response)["USDT_BTC"]
        except:
            return self.getFullTicker()
        return list(tickerData.values())[1:]

    def getLiveData(self, seperate=False):
        ob = self.getOrderBook()
        currentItem = ob[random.randint(0, 99)]

        if seperate:
            return (np.array(self.encoder.encode(ob)).flatten().tolist())+self.getFullTicker(), currentItem
        else:
            return currentItem+(np.array(self.encoder.encode(ob)).flatten().tolist())+self.getFullTicker()

    def getNextStateRewardV2(self, currentState, action):
        try:
            currentState = currentState[0]

            if action == 1:
                portfolioCapital = self.getPortfolioCaptal(self.getMarketPriceAverage())

                if currentState[2] > currentState[3]:
                    if self.fund >= currentState[0]*currentState[1]:
                        self.fund -= currentState[0]*currentState[1]
                        self.coin += currentState[1]
                    else:
                        return self.getLiveData(True)[0], self.idleReward, False
                elif currentState[3] > currentState[2]:
                    if self.coin >= currentState[1]: 
                        self.fund += currentState[0]*currentState[1]
                        self.coin -= currentState[1]
                    else:
                        return self.getLiveData(True)[0], self.idleReward, False

                newPortfolioCapital = self.getPortfolioCaptal(self.getMarketPriceAverage())

                d = False
                r = newPortfolioCapital - portfolioCapital


                if newPortfolioCapital <= 0:
                    r = -10
                    
                self.tradeIndex += 1 

                
                if self.tradeIndex >= 50:
                    self.resetEnv()
                    self.tradeIndex = 0 

                return self.getLiveData(True)[0], r*self.multiplier, d


            elif action==0:

                self.tradeIndex += 1    
                
                if self.tradeIndex >= 50:
                    self.resetEnv()
                    self.tradeIndex = 0 

                return self.getLiveData(True)[0], self.idleReward, False
        except:
            self.getNextStateReward(currentState, action)

    
    def getNextStateReward(self, currentState, action):
        try:
            currentState = currentState[0]

            if action == 1:
                portfolioCapital = self.getPortfolioCaptal(self.getMarketPriceAverage())

                if currentState[2] == 0:
                    if self.fund >= currentState[0]*currentState[1]:
                        self.fund -= currentState[0]*currentState[1]
                        self.coin += currentState[1]
                    else:
                        return self.getLiveData(), self.idleReward, False
                else:
                    if self.coin >= currentState[1]: 
                        self.fund += currentState[0]*currentState[1]
                        self.coin -= currentState[1]
                    else:

                        return self.getLiveData(), self.idleReward, False

                newPortfolioCapital = self.getPortfolioCaptal(self.getMarketPriceAverage())

                d = False
                r = newPortfolioCapital - portfolioCapital


                if newPortfolioCapital <= 0:
                    r = -10
                    
                self.tradeIndex += 1 

                
                if self.tradeIndex >= 50:
                    self.resetEnv()
                    self.tradeIndex = 0 

                return self.getLiveData(), r*self.multiplier, d


            elif action==0:

                self.tradeIndex += 1    
                
                if self.tradeIndex >= 50:
                    self.resetEnv()
                    self.tradeIndex = 0 

                return self.getLiveData(), self.idleReward, False
        except:
            self.getNextStateReward(currentState, action)

class MarketSimEnv(object):
    def __init__(self, startingFund=10000.0, startingBTCAmount=1.0, rewardMultiplier=1, idleReward=-0.004, dataObject=None):
        
        if dataObject:
            self.fund = dataObject['fund']
            self.coin = dataObject['coin']
            self.initFund = dataObject['initFund']
            self.initCoin = dataObject['initCoin']
            self.multiplier = dataObject['multiplier']
            self.idleReward = dataObject['idleReward']
            self.currentOrderBook = dataObject['currentOrderBook']
            self.tradeIndex = dataObject['tradeIndex']

            self.encoder = Autoencoder(4, 1, learningRate=0.01)
            self.encoder.train(self.getOrderBook(), epoch=10000)

            return
        self.fund = startingFund
        self.coin = startingBTCAmount
        
        self.initFund = self.fund
        self.initCoin = self.coin
        
        self.multiplier = rewardMultiplier
        self.idleReward = idleReward
        
        self.currentOrderBook = None
        self.tradeIndex = 0
        
        self.encoder = Autoencoder(4, 1, learningRate=0.01)
        self.encoder.train(self.getOrderBook(), epoch=10000)

    def export(self):
        return {
            "fund": self.fund,
            "coin": self.coin,
            "initFund": self.initFund,
            "initCoin": self.initCoin,
            "multiplier": self.multiplier,
            "idleReward": self.idleReward,
            "currentOrderBook": self.currentOrderBook,
            "tradeIndex": self.tradeIndex,
        }

    def initializeDB(self, total=100):
        with urllib.request.urlopen('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC') as response:
            orderBook = json.load(response)

        asks = orderBook["asks"]
        bids = orderBook["bids"]

        added = 0

        for i in asks:
            if added >= total:
                return

            price = float(i[0])
            amount = i[1]

            data = {
                "price":round(price, 2), 
                "coinAmount":round(amount, 10), 
                "orderBook":3,
                "type": 2,
            }

            r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 

            added += 1

        for i in bids:
            if added >= total:
                return
            
            price = float(i[0])
            amount = i[1]

            data = {
                "price":round(price, 2), 
                "coinAmount":round(amount, 10), 
                "orderBook":3,
                "type": 3,
            }

            r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 

            added += 1
        
    def getOrderBook(self):
        try:
            with urllib.request.urlopen('http://127.0.0.1:8000/api/USDT_BTC/orderBooks/') as response:
                orderBook = json.load(response)[0]
        except:
            return self.getOrderBook()
            
        for i in orderBook['asks']:
            i.append(1)
            i.append(1)
            
        for i in orderBook['bids']:
            i.append(1)
            i.append(1)
            
            
        currentOrderBook = orderBook['asks']+orderBook['bids'] 
        
        if len(currentOrderBook) <= 0:
            return self.getOrderBook()
        
        try:
            currentOrderBook[0]
        except IndexError:
            return self.getOrderBook()
        
        return currentOrderBook[:100]
    
    def getInitState(self):
        return self.getLiveData()
        
    def resetEnv(self):
        self.fund = self.initFund
        self.coin = self.initCoin
    
    def getPortfolioCaptal(self, marketPrice):
        return self.fund+self.coin*marketPrice
    
    def getMarketPriceAverage(self):
        try:
            with urllib.request.urlopen('http://127.0.0.1:8000/api/USDT_BTC/ticker?pk=3') as response:
                tickerData = json.load(response)
        except:
            return self.getMarketPriceAverage()
            
        return (float(tickerData["lowestAsk"])+float(tickerData["highestBid"]))/2 

    def getFullTicker(self):
        with urllib.request.urlopen('http://127.0.0.1:8000/api/USDT_BTC/ticker?pk=3') as response:
            tickerData = json.load(response)
        return list(tickerData.values())

    def getLiveData(self, seperate=False):
        ob = self.getOrderBook()
        if len(ob) < 100:
            print("Resetting Enviroment....")
            self.initializeDB(1)
            return self.getLiveData(seperate)
        currentItem = ob[random.randint(0, 99)]
        if seperate:
            return (np.array(self.encoder.encode(ob)).flatten().tolist())+self.getFullTicker(), currentItem
        else:
            return currentItem+(np.array(self.encoder.encode(ob)).flatten().tolist())+self.getFullTicker()

    def getNextStateRewardV2(self, currentState, action):
        try:
            currentState = currentState[0]

            if action == 1:
                portfolioCapital = self.getPortfolioCaptal(self.getMarketPriceAverage())

                if currentState[2] > currentState[3]:
                    if self.fund >= currentState[0]*currentState[1]:
                        self.fund -= currentState[0]*currentState[1]
                        self.coin += currentState[1]
                    else:
                        return self.getLiveData(True)[0], -1, False
                elif currentState[3] > currentState[2]:
                    if self.coin >= currentState[1]: 
                        self.fund += currentState[0]*currentState[1]
                        self.coin -= currentState[1]
                    else:

                        return self.getLiveData(True)[0], -1, False

                newPortfolioCapital = self.getPortfolioCaptal(self.getMarketPriceAverage())

                d = False
                r = newPortfolioCapital - portfolioCapital


                if newPortfolioCapital <= 0:
                    r = -10
                    
                self.tradeIndex += 1 

                
                if self.tradeIndex >= 50:
                    self.resetEnv()
                    self.tradeIndex = 0 

                return self.getLiveData(True)[0], r*self.multiplier, d


            elif action==0:

                self.tradeIndex += 1    
                
                if self.tradeIndex >= 50:
                    self.resetEnv()
                    self.tradeIndex = 0 

                return self.getLiveData(True)[0], self.idleReward, False
        except:
            self.getNextStateReward(currentState, action)

    
    def getNextStateReward(self, currentState, action):
        try:
            currentState = currentState[0]

            if action == 1:
                portfolioCapital = self.getPortfolioCaptal(self.getMarketPriceAverage())

                if currentState[2] == 0:
                    if self.fund >= currentState[0]*currentState[1]:
                        self.fund -= currentState[0]*currentState[1]
                        self.coin += currentState[1]
                    else:
                        return self.getLiveData(), self.idleReward, False
                else:
                    if self.coin >= currentState[1]: 
                        self.fund += currentState[0]*currentState[1]
                        self.coin -= currentState[1]
                    else:

                        return self.getLiveData(), self.idleReward, False

                newPortfolioCapital = self.getPortfolioCaptal(self.getMarketPriceAverage())

                d = False
                r = newPortfolioCapital - portfolioCapital


                if newPortfolioCapital <= 0:
                    r = -10
                    
                self.tradeIndex += 1 

                
                if self.tradeIndex >= 50:
                    self.resetEnv()
                    self.tradeIndex = 0 

                return self.getLiveData(), r*self.multiplier, d


            elif action==0:

                self.tradeIndex += 1    
                
                if self.tradeIndex >= 50:
                    self.resetEnv()
                    self.tradeIndex = 0 

                return self.getLiveData(), self.idleReward, False
        except:
            self.getNextStateReward(currentState, action)
