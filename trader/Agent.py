import time
import random
import numpy as np
from datetime import datetime

class CombinedAgent(object):
    def __init__(self, decisionPolicy, enviroment):
        self.dp = decisionPolicy
        self.ev = enviroment
        self.capitalHistory = []

    def adaptiveTrain(self, regressionTTL, decisionTTL, regressionBatchSize=20, resetTime=1, epoch=10):
        ttl = regressionTTL*60
        resetTime = resetTime*60
        self.currentState = [self.ev.getInitState()]
        self.gs = 0
        for i in range(epoch):
            startingTime = time.time()
            loopInitTime = time.time()
            print("Current timestamp: ", datetime.utcfromtimestamp(startingTime).strftime('%Y-%m-%d %H:%M:%S'))
            print("Training epoch", i)

            print("Pre-Training Regression Model...")
            print("Stage 0")
            while True:
                loopTime = time.time()
                inputDS = []
                outputDS = []
                for _ in range(regressionBatchSize):
                    currentData = self.ev.getLiveData(True)
                    inputDS.append(currentData[0])
                    outputDS.append(currentData[1])
                loss, ta = self.dp.trainRegression(inputDS, outputDS)
                if loopTime-startingTime >= ttl:
                    print("Time! Going to stage 1.")
                    break
                elif loopTime-loopInitTime >= resetTime:
                    print("Current timestamp: ", datetime.utcfromtimestamp(loopTime).strftime('%Y-%m-%d %H:%M:%S'), "Loss: ", loss)
                    print("Sample Transaction: ", ta)
                    loopInitTime = time.time()

            print("Training Decision Policy...")
            print("Stage 1")
            self.train(decisionTTL, resetTime/60)

            print("Training Regression Model...")
            print("Stage 2")
            startingTime = time.time()
            loopInitTime = time.time()
            while True:
                loopTime = time.time()
                inputDS = []
                outputDS = []
                for _ in range(regressionBatchSize):
                    currentData = self.ev.getLiveData(True)
                    inputDS.append(currentData[0])
                    outputDS.append(currentData[1])
                loss, ta = self.dp.trainRegression(inputDS, outputDS)
                if loopTime-startingTime >= ttl:
                    print("Time! Finishing...")
                    break
                elif loopTime-loopInitTime >= resetTime:
                    print("Current timestamp: ", datetime.utcfromtimestamp(loopTime).strftime('%Y-%m-%d %H:%M:%S'), "Loss: ", loss)
                    print("Sample Transaction: ", ta)
                    loopInitTime = time.time()

            print("Finished training epoch", i)

    def produceAction(self, rBC=False):
        if not rBC:
            self.dp.selectAction([self.ev.getLiveData(True)[0]], decay=False)
        else:
            actionSet = self.dp.selectAction([self.ev.getLiveData(True)[0]], decay=False)
            aIndx = actionSet[0]
            transaction = actionSet[1]
            slicedTransaction = [transaction[0][0:2:1]]
            transactionType = random.sample([[1,0],[0,1]], 1)
            published = np.concatenate((slicedTransaction, transactionType), axis=1)
            return aIndx, published

        return self.dp.selectAction([self.ev.getLiveData(True)[0]], decay=False)

    def train(self, ttl, resetTime=1):
        ttl = ttl*60
        resetTime = resetTime*60
        initTime = time.time()
        loopInitTime = time.time()
        print("Current timestamp: ", datetime.utcfromtimestamp(initTime).strftime('%Y-%m-%d %H:%M:%S'))

        self.currentState = [self.ev.getLiveData(True)[0]]

        d = False
        print("Starting Capital: ", self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
        print("Trading... Next reset in ", resetTime/60, " minutes!")

        
        while not d:
            
            a, t = self.dp.selectAction(self.currentState)
            s, r, d = self.ev.getNextStateRewardV2([t.tolist()[0]+self.currentState[0]], a)
            self.dp.updatePolicy(self.currentState, [s], a, r)
            self.currentState = [s]
            self.capitalHistory.append(self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
            
            loopTime = time.time()
            
            if loopTime-initTime >= ttl:
                print("Time! Going to Stage 2.")
                print("Ending Capital: ", self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
                break

            elif loopTime-loopInitTime >= resetTime:
                loopInitTime = time.time()
                print("Reset! Resetting... Current timestamp: ", datetime.utcfromtimestamp(loopTime).strftime('%Y-%m-%d %H:%M:%S'))
                print("Ending Capital: ", self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
                self.ev.resetEnv()
                print("Starting Capital: ", self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
                print("Trading... Next reset in ", resetTime/60, " minutes!")

class BidderAgent(object):
    def __init__(self, decisionPolicy, enviroment):
        self.dp = decisionPolicy
        self.ev = enviroment
        self.capitalHistory = []

    def train(self, ttl=10):
        ttl = ttl*60
        self.currentState = [self.ev.getInitState()]
        
        startingTime = time.time()
        currentTime = time.time()

        while currentTime-startingTime <= ttl:
            currentData = self.ev.getLiveData(True)
            self.dp.train([currentData[0]], currentData[1])
            currentTime = time.time()

    def predict(self, data):
        return self.dp.selectAction(data)
        
class BuyerAgent(object):
    def __init__(self, decisionPolicy, enviroment):
        self.dp = decisionPolicy
        self.ev = enviroment
        self.capitalHistory = []

    def train(self, ttl=10, resetTime=1):
        ttl = ttl*60
        resetTime = resetTime*60
        initTime = time.time()
        loopInitTime = time.time()
        print("Current timestamp: ", datetime.utcfromtimestamp(initTime).strftime('%Y-%m-%d %H:%M:%S'))

        self.currentState = [self.ev.getInitState()]

        d = False
        print("Starting Capital: ", self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
        print("Trading... Next reset in ", resetTime/60, " minutes!")

        while not d:
            a = self.dp.selectAction(self.currentState)
            s, r, d = self.ev.getNextStateReward(self.currentState, a)
            self.dp.updatePolicy(self.currentState, a, r, [s])
            self.currentState = [s]
            self.capitalHistory.append(self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
            
            loopTime = time.time()
            
            if loopTime-initTime >= ttl:
                print("Time! Shutting Down.")
                print("Ending Capital: ", self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
                return
            elif loopTime-loopInitTime >= resetTime:
                loopInitTime = time.time()
                print("Reset! Resetting... Current timestamp: ", datetime.utcfromtimestamp(loopTime).strftime('%Y-%m-%d %H:%M:%S'))
                print("Ending Capital: ", self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
                self.ev.resetEnv()
                print("Starting Capital: ", self.ev.getPortfolioCaptal(self.ev.getMarketPriceAverage()))
                print("Trading... Next reset in ", resetTime/60, " minutes!")
                
    def predict(self, currentState):
        return self.dp.selectAction(currentState)
        