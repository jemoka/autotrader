import tensorflow as tf
import numpy as np
import random
import os

class CombinedAgentPolicy(object):
    def __init__(self, hiddenla1=5, hiddenla2=10, hiddenlb1=5, discountFactor=0.3, tLearningRate=0.01, qLearningRate=0.005, epilson=1.0, epilsonDecay=10, logging="#NO#~", restoreObject=None):
        if restoreObject:
            dataObject = restoreObject[1]
            self.hiddenla1 = dataObject['hiddenla1']
            self.hiddenla2 = dataObject['hiddenla2']
            self.hiddenlb1 = dataObject['hiddenlb1']
            self.tLr = dataObject['tLearningRate']
            self.qLr = dataObject['qLearningRate']
            self.discountFactor = dataObject['discountFactor']
            self.logdir = dataObject['logging']
        else:
            self.hiddenla1 = hiddenla1
            self.hiddenla2 = hiddenla2
            self.hiddenlb1 = hiddenlb1
            self.tLr = tLearningRate
            self.qLr = qLearningRate
            self.discountFactor = discountFactor
            self.logdir = logging

        self.epilson = epilson
        self.epilsonDecay = epilsonDecay
        self.regressionInput = tf.placeholder(tf.float32, [None, 109], "inputData")

        self.expectedQValues = tf.placeholder(tf.float32, [2], "expectedQ")
        self.templateTransaction = tf.placeholder(tf.float32, [None, 4], "templateTransaction")

        self.rFactor = tf.placeholder(tf.float32, [None, 1], "randomnessFactor")

        wa1 = tf.Variable(tf.random_uniform([109, self.hiddenla1], maxval=0.01), name="regressionWeight1") # weight 1 
        ha1 = tf.matmul(self.regressionInput, wa1, name="regressionHidden1")

        wa2 = tf.Variable(tf.random_uniform([self.hiddenla1, self.hiddenla2], maxval=0.01), name="regressionWeight2") # weight 1 
        ha2p = tf.matmul(ha1, wa2, name="regressionHidden2")

        rFactor = tf.tile(self.rFactor, [tf.shape(ha2p)[0], 1]) 

        ha2 = tf.concat([ha2p, rFactor], 1)
        wa3 = tf.Variable(tf.random_uniform([self.hiddenla2+1, 4], maxval=0.01), name="regressionWeight3") # weight 1 

        linearActivation = tf.Variable(tf.constant(0.01, shape=[4]), name="regressionActivation")

        self.transaction = tf.add(tf.matmul(ha2, wa3), linearActivation, name="generatedTransaction")
        self.decisionInput = tf.concat([self.transaction, self.regressionInput], 1)

        wb1 = tf.Variable(tf.random_uniform([113, self.hiddenlb1], maxval=0.01), name="decisionWeight1") # weight 1 
        hb1 = tf.matmul(self.decisionInput, wb1, name="decisionHidden1")

        wb2 = tf.Variable(tf.random_uniform([self.hiddenlb1, 2], maxval=0.01), name="decisionWeight2") # final weight
        self.q = tf.nn.leaky_relu(tf.matmul(hb1, wb2, name="calculatedQ"))

        self.tLoss = tf.square(self.templateTransaction-self.transaction, name="tLoss")
        self.qLoss = tf.square(self.expectedQValues-self.q, name="qLoss")

        self.lr_gs = global_step = tf.Variable(0, trainable=False, name="gs")
        learning_rate = tf.train.exponential_decay(self.tLr, global_step, 100, 0.95, staircase=True)
        
        # self.qTrainingOperation = tf.train.AdagradOptimizer(self.qLr).minimize(self.qLoss, name='qTrain', var_list=[wb1, bb1, wb2, bb2]) # training operation
        self.tTrainingOperation = tf.train.AdagradOptimizer(learning_rate).minimize(self.tLoss, name='tTrain', var_list=[wa1, wa2, wa3], global_step=self.lr_gs)
        self.qTrainingOperation = tf.train.AdagradOptimizer(self.qLr).minimize(self.qLoss, name='qTrain', var_list=[wb1, wb2]) # training operation


        self.sess = tf.Session()
        self.globalStep = 0

        if self.logdir != "#NO#~":
            self.trainingFilewriter = tf.summary.FileWriter(self.logdir, self.sess.graph)
            self.tLossSummary = tf.summary.scalar("tLoss", tf.reduce_mean(self.tLoss))
            self.qLossSummary = tf.summary.scalar("qLoss", tf.reduce_mean(self.qLoss))
            self.summaryOperation = tf.summary.merge_all()

        self.saver = tf.train.Saver()

        if restoreObject:
            self.saver.restore(self.sess, restoreObject[0])
        else:
            self.sess.run(tf.global_variables_initializer())

    def updatePolicy(self, currentState, nextState, action, reward, randomness=None):
        self.globalStep += 1
        if not randomness:
            uniform = random.uniform(1, 1)
        else:
            uniform = randomness
        implicatedQVals = self.sess.run(self.q, feed_dict={self.regressionInput: currentState, self.rFactor: [[uniform]]})
        implicatedNextQVals = self.sess.run(self.q, feed_dict={self.regressionInput: nextState, self.rFactor: [[uniform]]})
        nextAction = np.argmax(implicatedNextQVals)

        implicatedQVals[0, action] = reward + self.discountFactor*implicatedNextQVals[0, nextAction]

        if self.logdir != "#NO#~":
            _, qSummary = self.sess.run([self.qTrainingOperation, self.qLossSummary], feed_dict={self.regressionInput: currentState, self.expectedQValues: implicatedQVals[0], self.rFactor: [[uniform]]})
            self.trainingFilewriter.add_summary(qSummary, self.globalStep)
        else:
            self.sess.run(self.qTrainingOperation, feed_dict={self.regressionInput: currentState, self.expectedQValues: implicatedQVals[0], self.rFactor: [[uniform]]})

    def selectAction(self, currentState, decay=True, randomness=None):
        if not randomness:
            uniform = random.uniform(1, 1)
        else:
            uniform = randomness
        implicatedQVals, transaction = self.sess.run([self.q, self.transaction], feed_dict={self.regressionInput: currentState, self.rFactor: [[uniform]]})
        actionIndex = np.argmax(implicatedQVals)
        if decay:
            if self.globalStep != 1 and self.globalStep % self.epilsonDecay:
                self.epilson = self.epilson*0.95
        randomSeed = random.uniform(0, 1)
        if randomSeed <= self.epilson:
            actionIndex = random.choice([0, 1])
        return actionIndex, transaction

    def trainRegression(self, inputData, expectedOutput, randomness=None):
        if not randomness:
            uniform = random.uniform(1, 1)
        else:
            uniform = randomness
        self.globalStep += 1
        if self.logdir != "#NO#~":
            _, summary, ta, loss = self.sess.run([self.tTrainingOperation, self.tLossSummary, self.transaction, self.tLoss], feed_dict={self.regressionInput: inputData, self.templateTransaction: expectedOutput, self.rFactor: [[uniform]]})
            self.trainingFilewriter.add_summary(summary, self.globalStep)
        else:
            _, ta, loss = self.sess.run([self.tTrainingOperation, self.transaction, self.tLoss], feed_dict={self.regressionInput: inputData, self.templateTransaction: expectedOutput, self.rFactor: [[uniform]]})
        return loss, ta

    def export(self, directory, id_tag):
        policyDirectory = self.saver.save(self.sess, os.path.join(directory, id_tag+"_POLICY.ckpt"))

        return policyDirectory, {
            'hiddenla1': self.hiddenla1,
            'hiddenla2': self.hiddenla2,
            'hiddenlb1': self.hiddenlb1,
            'tLearningRate': self.tLr,
            'qLearningRate': self.qLr,
            'discountFactor': self.discountFactor,
            'logging': self.logdir,
        }
        

        

class DeepQLearningPolicy(object):
    def __init__(self, actions=None, inputDim=None,  uncertainty=0.95, discountFactor=0.3, hiddenLayer1Dim=4, learningRate=0.01, logging="#NO#~", restoreObject=None):
        
        if restoreObject:
            dataObject = restoreObject[1]
            self.epsilon = dataObject['uncertainty']
            self.gamma = dataObject['discountFactor']
            self.actionSpace = dataObject['actionSpace']
            self.outputDim = len(self.actionSpace)
            self.inputDim = dataObject['inputDim']
            self.learningRate = dataObject['learningRate']
            self.hiddenLayer1Dim = dataObject['hiddenLayer1Dim']
            self.log = dataObject['logging']
            self.globalStep = dataObject['globalStep']
            learningRate = dataObject['learningRate']
            hiddenLayer1Dim = dataObject['hiddenLayer1Dim']
        else:
            self.epsilon = uncertainty
            self.gamma = discountFactor
            self.actionSpace = actions
            self.learningRate = learningRate
            self.hiddenLayer1Dim = hiddenLayer1Dim
            self.outputDim = len(actions)
            self.inputDim = inputDim
            self.log = logging
            self.globalStep = 0

        
        self.x = tf.placeholder(tf.float32, [None, self.inputDim], "state") # the state (probably OneHot)
        self.y = tf.placeholder(tf.float32, [self.outputDim], "expectedQ") # q-values output ("correct")
        
        w1 = tf.Variable(tf.random_uniform([self.inputDim, hiddenLayer1Dim]), name="weight1") # weight 1 
        b1 = tf.Variable(tf.constant(0.1, shape=[hiddenLayer1Dim]), name="bias1") # bias 1 maybe tf.constant(0.1 shape=??)
        h1 = tf.add(tf.matmul(self.x, w1), b1, name="hidden1")
        
        
        w3 = tf.Variable(tf.random_uniform([hiddenLayer1Dim, self.outputDim]), name="weight3") # final weight
        b3 = tf.Variable(tf.constant(0.1, shape=[self.outputDim]), name="bias3") # final bias maybe tf.constant(0.1 shape=??)
        
        self.q = tf.add(tf.matmul(h1, w3), b3, name="calculatedQ") # q-values output (inferred)
        
        loss = tf.square(self.y-self.q, name="loss") # sets loss as square errors

        # loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y, logits=self.q, name="loss")

#         loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y, logits=self.q)
        self.trainingOperation = tf.train.AdagradOptimizer(learningRate).minimize(loss) # training operation

        self.sess = tf.Session() # current session
       
        self.saver = tf.train.Saver()

        if self.log != "#NO#~":
            self.trainingFilewriter = tf.summary.FileWriter(self.log, self.sess.graph)
            tf.summary.scalar("loss", tf.reduce_mean(loss, name="average_loss"))
            self.summaryOperation = tf.summary.merge_all()
        
        if restoreObject:
            self.saver.restore(self.sess, restoreObject[0])
        else:
            self.sess.run(tf.global_variables_initializer()) # run global variables initializer
    
    def export(self, directory, id_tag):
        policyDirectory = self.saver.save(self.sess, os.path.join(directory, id_tag+"_POLICY.ckpt"))

        return policyDirectory, {
            'uncertainty': self.epsilon,
            'discountFactor': self.gamma,
            'actionSpace': self.actionSpace,
            'inputDim': self.inputDim,
            'learningRate': self.learningRate,
            'hiddenLayer1Dim': self.hiddenLayer1Dim,
            'logging': self.log,
    	    'globalStep': self.globalStep,
        }

    def selectAction(self, currentState):
        randomThreshold = random.randint(1, 500)
        if 350 <= randomThreshold <= 355:
            actionSelected = self.actionSpace[random.randint(0, self.outputDim-1)]
        else:
            implicatedQVals = self.sess.run(self.q, feed_dict={self.x: currentState})
            actionIndex = np.argmax(implicatedQVals)
            actionSelected = self.actionSpace[actionIndex]
        return actionSelected
    
    def updatePolicy(self, state, action, reward, nextState):
        self.globalStep += 1
        implicatedQVals = self.sess.run(self.q, feed_dict={self.x: state})
        implicatedNextQVals = self.sess.run(self.q, feed_dict={self.x: nextState})
        
        
        currentActionIndex = self.actionSpace.index(action)
        nextActionIndex = np.argmax(implicatedNextQVals)
        
        implicatedQVals[0, currentActionIndex] = reward + self.gamma*implicatedNextQVals[0, nextActionIndex]

        if self.log != "#NO#~":
            _, summary = self.sess.run([self.trainingOperation, self.summaryOperation], feed_dict={self.x: state, self.y: implicatedQVals[0]})
            self.trainingFilewriter.add_summary(summary, self.globalStep)
        else:
            self.sess.run(self.trainingOperation, feed_dict={self.x: state, self.y: implicatedQVals[0]})
        


        
class DeepRegressionPolicy(object):
    def __init__(self, inputDim=None, hiddenl1=5, hiddenl2=10, learningRate=0.1, logging="#NO#~", restoreObject=None):
        if restoreObject:
            dataObject = restoreObject[1]
            self.inputDim = dataObject['inputDim']
            self.hiddenl1 = dataObject['hiddenl1']
            self.hiddenl2 = dataObject['hiddenl2']
            self.learningRate = dataObject['learningRate']
            self.log = dataObject['log']
        else:
            self.inputDim = inputDim
            self.hiddenl1 = hiddenl1
            self.hiddenl2 = hiddenl2
            self.learningRate = learningRate
            self.log = logging
        
        self.x = tf.placeholder(tf.float32, [None, self.inputDim], "inputData")
        self.y = tf.placeholder(tf.float32, [3], "expectedTransaction")

        w1 = tf.Variable(tf.random_uniform([self.inputDim, self.hiddenl1]), name="weight1") # weight 1 
        b1 = tf.Variable(tf.constant(0.1, shape=[self.hiddenl1]), name="bias1") # bias 1 maybe tf.constant(0.1 shape=??)
        h1 = tf.add(tf.matmul(self.x, w1), b1, name="hidden1")

        w2 = tf.Variable(tf.random_uniform([self.hiddenl1, self.hiddenl2]), name="weight2") # weight 1 
        b2 = tf.Variable(tf.constant(0.1, shape=[self.hiddenl2]), name="bias2") # bias 1 maybe tf.constant(0.1 shape=??)
        h2 = tf.add(tf.matmul(h1, w2), b2, name="hidden2")

        w3 = tf.Variable(tf.random_uniform([self.hiddenl2, 3]), name="weight3") # weight 1 
        b3 = tf.Variable(tf.constant(0.1, shape=[3]), name="bias3") # bias 1 maybe tf.constant(0.1 shape=??)
        self.yhat = tf.nn.relu(tf.add(tf.matmul(h2, w3), b3), name="transaction")

        loss = tf.reduce_mean(tf.square(self.y-self.yhat, name="loss")) # sets loss as mean square errors

#         loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y, logits=self.q)
        self.trainingOperation = tf.train.AdagradOptimizer(learningRate).minimize(loss) # training operation

        self.sess = tf.Session() # current session
       
        self.saver = tf.train.Saver()

        self.globalStep = 0

        if self.log != "#NO#~":
            self.trainingFilewriter = tf.summary.FileWriter(self.log, self.sess.graph)
            tf.summary.scalar("loss", tf.reduce_mean(loss, name="average_loss"))
            self.summaryOperation = tf.summary.merge_all()
        
        if restoreObject:
            self.saver.restore(self.sess, restoreObject[0])
        else:
            self.sess.run(tf.global_variables_initializer()) # run global variables initializer

    def train(self, inputData, expectedOutput):
        self.globalStep += 1
        if self.log != "#NO#~":
            _, summary = self.sess.run([self.trainingOperation, self.summaryOperation], feed_dict={self.x: inputData, self.y: expectedOutput})
            self.trainingFilewriter.add_summary(summary, self.globalStep)
        else:
            self.sess.run(self.trainingOperation, feed_dict={self.x: inputData, self.y: expectedOutput})

    def selectAction(self, inputData):
        return self.sess.run(self.yhat, feed_dict={self.x: inputData})

    def export(self, directory, id_tag):
        policyDirectory = self.saver.save(self.sess, os.path.join(directory, id_tag+"_POLICY.ckpt"))

        return policyDirectory, {
            'inputDim': self.inputDim,
            'hiddenl1': self.hiddenl1,
            'hiddenl2': self.hiddenl2,
            'learningRate': self.learningRate,
            'log': self.log,
        }
