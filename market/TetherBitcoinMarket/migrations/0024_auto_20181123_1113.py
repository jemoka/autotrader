# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-23 19:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TetherBitcoinMarket', '0023_auto_20181123_1107'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ask',
            name='coinAmount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
        migrations.AlterField(
            model_name='ask',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
        migrations.AlterField(
            model_name='bid',
            name='coinAmount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
        migrations.AlterField(
            model_name='bid',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
        migrations.AlterField(
            model_name='market',
            name='baseVolume',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
        migrations.AlterField(
            model_name='market',
            name='name',
            field=models.CharField(default='5273886a-2119-499c-9323-cb417e2c0b8d', max_length=200),
        ),
        migrations.AlterField(
            model_name='market',
            name='quoteVolume',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
        migrations.AlterField(
            model_name='orderbook',
            name='name',
            field=models.CharField(default='56de0f04-c47f-4e51-b81b-bebd0d04cf7e', max_length=200),
        ),
        migrations.AlterField(
            model_name='trade',
            name='coinAmount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
        migrations.AlterField(
            model_name='trade',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
    ]
