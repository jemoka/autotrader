# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-22 19:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TetherBitcoinMarket', '0004_auto_20181122_1138'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='isSell',
        ),
        migrations.AddField(
            model_name='order',
            name='orderType',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='market',
            name='name',
            field=models.CharField(default='56d6f7d3-279f-4fc1-8132-82a7a4fca473', max_length=200),
        ),
        migrations.AlterField(
            model_name='orderbook',
            name='name',
            field=models.CharField(default='74993569-3584-48b5-a618-ee70dbba5a58', max_length=200),
        ),
    ]
