from rest_framework import serializers
from .models import Market, OrderBook, Bid, Ask, Trade

class BidSerializer(serializers.ModelSerializer):

    def to_representation(self, obj):
        data = super().to_representation(obj)
        return list(data.values())[:-1]
    
    class Meta:
        model = Bid
        fields = ("price", "coinAmount", "orderBook")

class AskSerializer(serializers.ModelSerializer):

    def to_representation(self, obj):
        data = super().to_representation(obj)
        return list(data.values())[:-1]

    class Meta:
        model = Ask
        fields = ("price", "coinAmount", "orderBook")

class OrderBookSerializer(serializers.ModelSerializer):
    asks = AskSerializer(many=True, read_only=True)
    bids = BidSerializer(many=True, read_only=True)

    class Meta:
        model = OrderBook
        fields = ("id", "name", "bids", "asks")

class TradeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Trade
        fields = ("id", "price", "coinAmount", "timestamp")

class MarketSerializer(serializers.ModelSerializer):

    trades = TradeSerializer(many=True, read_only=True)
    orderBooks = OrderBookSerializer(many=True, read_only=True)

    class Meta:
        model = Market
        fields = ("id", "name", "currentDate", "baseVolume", "quoteVolume", "orderBooks", "trades")


