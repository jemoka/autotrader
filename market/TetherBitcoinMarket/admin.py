from django.contrib import admin
from .models import Market, OrderBook, Bid, Ask, Trade

admin.site.register(Market)
admin.site.register(OrderBook)
admin.site.register(Bid)
admin.site.register(Ask)
admin.site.register(Trade)
