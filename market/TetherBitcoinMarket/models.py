from django.db import models
import uuid

class Market(models.Model):
    name = models.CharField(max_length=200, default=str(uuid.uuid4()))
    currentDate = models.DateTimeField(auto_now=True)
    baseVolume = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    quoteVolume = models.DecimalField(max_digits=20, decimal_places=10, default=0)

    def __str__(self):
        return "Tether Bitcoin Market \""+self.name+"\""

class OrderBook(models.Model):
    name = models.CharField(max_length=200, default=str(uuid.uuid4()))
    market = models.ForeignKey(Market, null=True, blank=False, related_name="orderBooks")

class Bid(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    coinAmount = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    orderBook = models.ForeignKey(OrderBook, null=True, blank=False, related_name="bids")

    def __str__(self):
        return "bid @"+str(self.price)+" for "+str(self.coinAmount)

class Ask(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    coinAmount = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    orderBook = models.ForeignKey(OrderBook, null=True, blank=False, related_name="asks")

    def __str__(self):
        return "ask @"+str(self.price)+" for "+str(self.coinAmount)

class Trade(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    coinAmount = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    timestamp = models.DateTimeField(auto_now=True)
    market = models.ForeignKey(Market, null=True, blank=False, related_name="trades")

    def __str__(self):
        return "trade @"+str(self.price)+" for "+str(self.coinAmount)



