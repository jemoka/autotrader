from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^markets/', views.MarketView.as_view(), name="marketView"),
    url(r'^orderBooks/', views.OrderBookView.as_view(), name="orderBookView"),
    url(r'^orders/', views.OrderView.as_view(), name="orderBookView"),
    url(r'^ticker/', views.MarketTickerView.as_view(), name="ticker"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
