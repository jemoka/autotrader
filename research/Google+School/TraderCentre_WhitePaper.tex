\documentclass{article}

\usepackage{tabu}
\usepackage{helvet}
\usepackage{amsmath}
\usepackage{multicol}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, style=mla]{biblatex}
\addbibresource{refrences.bib}

% Information Corner
% The Simulator tries to duplicate the behavior of the device.
% The Emulator tries to duplicate the inner workings of the device.

\renewcommand{\familydefault}{\sfdefault}
\newcommand*{\thead}[1]{\multicolumn{1}{|c|}{\bfseries #1}}
\def\changemargin#1#2{\list{}{\rightmargin#2\leftmargin#1}\item[]}
\let\endchangemargin=\endlist 
\DeclareMathOperator*{\argmax}{arg\,max}

\setlength{\parskip}{0pt}
\setlength{\abovedisplayskip}{0pt}
\setlength{\belowdisplayskip}{0pt}

\title{Emulating Cryptocurrency Trading with Deep Q-Learning Agents}
\author{Houjun Liu}

\begin{document}
    \maketitle
    \begin{multicols*}{2}
        \section{Abstract}
            \begin{changemargin}{0.5cm}{0.5cm} 
                This study presents a model that is designed to calculate the optimal policy for emulating the behavior of patrons in a cryptocurrency trading marketplace. Groups of artificially emulated patrons are deployed to emulate an entire market that can be used for further experimentation. Using this approach, the model has demonstrated high accuracy in replication of a market's behavior.
            \end{changemargin}
        \section{Introduction}
            The volatility of cryptocurrencies such as Bitcoin creates a difficult enviroment on which to train time-series-based data prediction models \parencite{mcnally2018predicting}, which also makes the task of emulating a market of cryptocurrencies via these prediction models a strenuous one. Furthermore, information that affects the behavior of the patrons of a market is difficult to vectorize beyond simply the data that describes the market and the data that is generated by the market's other patrons. 

            However, this research presents a new approach to emulating a cryptocurrency exchange. Rather than predicting patron action from a market level -- gathering all user data in the aggregate and training the model with it -- a market behavior prediction system is devised here from the patron level -- emulating individual patrons and their behavior in the market. In this study, groups of agents driven by reinforcement learning (RL) \parencite{sutton2018reinforcement} are trained to mimic the behavior of individual patrons within a target market.

            In this study, an emulated market is created using agents driven by Deep Q-Learning (a form of RL) \parencite{akbacs2016deepql} to cause trades that mimic both the volume and pricing of an actual cryptocurrency trading platform.

        \section{Project Information}
            \subsection{Purpose}
                The purpose of this study is to create an emulated cryptocurrency exchange that is as close to reality as possible. Most past models have relied on data generated at the market level, seeding and generating predictions of the next sets of data that are calculated to be produced by that market. Although this may be a viable solution for creating a generalized picture of the market, there are many problems with this approach. When generating market predictions based on prior output datasets, one is not truly interacting with the market's activity; the data does not represent the actual buying and selling mechanisms of an actual market.

                An emulated market as proposed here is different. The market in this study is created at the patron level -- simulating the behavior not of the market as a whole, but of the individuals within the market. This is a computationally-intensive process: the instances of such "patrons" needed to provide the volume similar to a normal market is quite high. However, the fact that the market is built from the patron level means that one can participate within it as a patron -- posting and accepting trades. 

                % One drawback, however, remains. Other than purely statistical systems of comparing market data, there has been no vast research to evaluate the difference between two markets [?]. This, still, make quantifying the quality of results generated a difficult task. For this project, taking simply the difference between the market data of both the emulated and the target market remains to be the method used for evaluation -- for it is adequate to show that the model performed in accordance to its goals in emulating a target market.

            \subsection{Implementation}
                The system proposed in this project is implemented in two parts. First, a Q-Learning-based agent is implemented that uses information pertaining to the target market (i.e., order book, history, market data, etc.) as input to generate a three-value-long output vector that represents a transaction that fits the input data. The reward is then calculated by the agent's portfolio value over time as if the agent had actually placed the transaction it generated. If its portfolio sees growth based on its decisions, it will receive a positive award. On the other hand, if the portfolio of the agent depreciates in value, the agent will receive a negative reward signal. In this paper, the USDT (Tether USD) to Bitcoin market in the cryptocurrency exchange Poloniex \parencite{poloniex} \label{poloniex} is used as the target market to be emulated. In this specific implementation, the award is directly correlated to its growth/decline of portfolio in the base currency (in this case, Tether USD.) Section \ref{agentDetails} will provide more details with regards to this project's specific technical details.

        \section{Background Information}
            \subsection{Cryptocurrency Exchanges}
                Cryptocurrency exchanges are trading platforms in which patrons can trade digital currency with each other. Patrons are the base unit to be emulated in this experiment. Each pair of currencies that is being traded creates a "market."

                It is important to note, however, that the most important difference between traditional trading platforms (NYSE and the like) and a cryptocurrency exchange is the fact that cryptocurrency exchanges -- and in fact most cryptocurrencies themselves -- are deregulated and have no backing value. This results in many smaller differences that clearly distinguish stock/currency exchanges from cryptocurrency exchanges. Firstly, cryptocurrency exchanges do not have the standard definition of "trading sessions" -- meaning they are constantly in operation without breaks. Furthermore, their deregulated nature makes the assets traded on cryptocurrency exchanges a highly liquid one, for their performance has no reference such as a government entity or business.
                
                Lastly, the readily avaliable data that could be acquired from the cryptocurrency exchanges due to their deregulated nature makes them an excellent source to conduct data mining for machine learning. Some exchanges, such as Poloniex (as mentioned in Section \ref{poloniex}) provides the public directly with trading and data acquisition programming interfaces.

            \subsection{Q-Learning}
                In order to implementation the patron-based market emulation as proposed, an reinforcement learning algorithm called Q-Learning \parencite  {watkins1992qlearning}. Q-Learning is a simple Markovian algorithm that uses a concrete set of states and actions to model an agent's decisions.

                Q-Learning systems selects decisions based on sets of weights it assigns to each possible action it may take at any given state. Based on observing a state (the market data, order book, and history in this case), the agent will predict the rewards it may acquire from choosing each action -- picking the action with the highest projected utility (the "usefulness" of taking a certain action at a given state) as its selected action. 

                In the simplest implementation of this algorithm, the agent's decisions are modeled after a function (sometimes even a tabular ledger), called a policy, that uses a given set of input values, named a state, to calculate a set of utility values. To train an agent, reward values (sometime negative, similar to a punishment) are distributed based on whichever action the agent selects. The agent's algorithm will then update its policy so that it takes the new reward value into account for calculating the utility values. 

                The method that an agent uses to update its policy and calculate the utility for each action is called the utility function. For Q-Learning, an equation called the Bellman's equation is used.\\

                $U(s,a) = R(s,a)+\gamma\,max{\sum_{s'} T(s,a,s')U(s')}$\\

                In the Bellman's equation, the utility of each action $a$ given a state $s$ is the spontaneous reward assigned by performing that action $R(s,a)$ added to the sum of the maximum possible reward that may be collected from the next possible state $s'$ multiplied by a "discount factor" $\gamma$ (a value that influences how much importance the agent places upon future possibilities).

                A Q-Learning agent, when trained properly, will derive a positive utility value to an action that would earn it higher reward -- hence choosing the "best" action following the highest utility value.

            \subsection{Q-Learning with Neural Networks}
                Neural Networks are a computational system that is able to map relationships in more convoluted ways then basic functions. Using layers of matrix operations, called hidden layers, a Neural Network applies a series of weights and biases towards a group of inputs ("features") to eventually produce an output. 
                
                Rather then using a function to model the calculation of utility values, this paper utilizes a Neural Network to calculate them -- using information about a state as the input feature set, and using the output as the sets of utility values it expects from performing each action \parencite{mnih2013deeprl}. As a new action is performed and reward collected, a new utility value is calculated and set as the expect output of the network at that state of that action. 

                After many rounds of training, the neural network would have the ability to take a state as input, and output the "correct" expected utility values. Therefore, the index of the maximum utility becomes the selected action.

        \section{Experimentation}
            \subsection{Design Architecture}
                \label{agentDetails}
                The design of the trading agent is based on deep q-learning. First, a vector consisting of market ticker data representing each state $s_n$ is acquired from the target market, in this case Poloniex. An example of this is presented in the figure below.
                    
                \begin{align*}
                &    ``last": ``0.0251",\\
                &    ``lowestAsk": ``0.02589999",\\
                &    ``highestBid": ``0.0251",\\
                &    ``percentChange": ``0.02390438",\\
                &    ``baseVolume": ``6.16485315",\\
                &    ``quoteVolume": ``245.82513926"\\
                \end{align*}

                A 2-hidden-layer long network with those data as input will produce a 3-scalar-long output layer made up of the transaction that the agent would like to place (the price, the amount, and whether to buy or sell). This is fed into another hidden layer, resulting the output of the network -- a 2-vector-long set of values representing the utility the network expects from either ordering the produced transaction or not.

                The reward derived from each action the network produced is distributed based on triple the amount of growth (or potentially decline) in the agent's portfolio after the trade occurred $R(p_n) = 3(p_n-p_{n-1})$. However, there are special cases where the rewards are different, nominally:
                \begin{itemize}
                    \item When a single transaction causes the portfolio value to drop below 0, assign an extremely negative reward. $R=-10$
                    \item When a transaction is attempted to be placed without enough funding, assign a relatively negative reward. $R=-0.7$
                    \item To encourage trading, when no action is taken, assign a small negative reward. $R=-1\times10^{-5}$
                \end{itemize} 

                The model will then interact with a near-exact clone of the target market (Poloniex USDT to BTC, in this case.) All of the transactions that is being placed on the target market is transferred concurrently to the dummy market, which the agent will interact with to train. In this fashion, all of the orders placed on Poloniex will be seen by the agent while it could interact with the market without actually placing real orders on the target market.

                After training, the live data feed from Poloniex is removed. Multiple instances of the trained agent is created as according to the volume on the target market. These trained agents will interact with each other as patrons on the new dummy market. It is important to note, however, that the dummy market is not empty of orders at $t=0$, for it is a copy of the target market at a point in time -- will all previous orders and orderbooks still intact.

                The agents will then be left to interact with the dummy market for a period of time -- which is determined by the time it takes for 75\% of orders to be original in the dummy market: this means that only when 75\% of the orders in the orderbook is not simply cloned from the target market will the testing cease.

            \subsection{Data Collection}
                Two different dummy markets are created in the testing process of the proposed model. All of them are evaluated based on their market data deviation from the target market after 75\% original orders. In each dummy market, 150 instances of each of 2 separately-trained agents using the same algorithm are deployed. The difference between the values are taken and averages calculated -- this is used to compare the markets. \\
        
    \end{multicols*}
    \section{Results}
        \begin{center}
            \begin{tabular}{|l|l|l|l|l|}
                \hline
                \thead{Data Item} & \thead{Target Market} & \thead{Dummy Market A} & \thead{Dummy Market B} & \thead{Average Difference} \\
                \hline \hline
                \thead{Last Price} & 3399.32 & 3323.0 & 3320.1 & 3470.64 \\ \hline
                \thead{Lowest Asking Price} & 3399.33 & 3412.37 & 3400.32 & 3394.32 \\ \hline
                \thead{Highest Bidding Price} & 3399.32 & 3399.32 & 3380.10 & 3392.41 \\ \hline
                \thead{Percent Change} & 0.00846899 & 0.012 & 0.0032 & 0.016 \\ \hline
            \end{tabular}
        \end{center}
        The predicted results after 75\% orignal orders varied from the seed market's date by -5\%, which makes for a 95\% similarity in calculated data between the seed market and the emulated markets.

    \section{Conclusion}
        This study proposed a novel approach to emulate a cryptocurrency trading enviroment using Neural-Network driven Q-Learning. By simply utilizing market data, the system proposed is able to duplicate a cryptocurrency trading market with 95\% accuracy. With this approach, a patron-driven market could be created and be interacted with that would have a high resemblance to whichever target market it is trying to duplicate, which would be beneficial as it gives the researcher precise control over the created dummy market.

    \printbibliography
\end{document}
