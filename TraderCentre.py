import trader
import time
import uuid
import gc
import os
import sys
import platform
import psutil
import logging
import requests
from decimal import Decimal
from datetime import datetime
from trader.commons import save
from trader.Agent import BuyerAgent
from trader.Agent import BidderAgent
from trader.Agent import CombinedAgent
from trader.Enviroment import PoloniexEnv
from trader.Enviroment import MarketSimEnv
from trader.Policy import DeepQLearningPolicy
from trader.Policy import DeepRegressionPolicy
from trader.Policy import CombinedAgentPolicy


print("Welcome to the Trader Centre Expermentation Enviroment")
print("The server time is currently at", time.time(),"UNIX")
print(datetime.utcfromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))

print("""
TraderCenter (Autotrader) Copyright (C) 2018  Houjun Liu
This program comes with ABSOLUTELY NO WARRANTY. 
This is free software, and you are welcome to redistribute it
under certain conditions
""")

input("PRESS ENTER TO CONTINUE")

command = "0"


def clear():
    if platform.system() == "Windows":
        os.system('cls')
    else:
        os.system('clear')

def agentFC(name, agent, env, policy, dir, initState=False):
    clear()
    if initState:
        print("""
    Trader Centre v0.1~DEC2_1
    -----------------------------------------------
    Combined Agent %(name)s Options
    t - Adaptively Train Agent
    f - Train Agent Decision Model Only
    This is the Initialization Menu.
    More options will be enabled after the agent is trained initially.
    -----------------------------------------------
        """ % {'name': name})
    else:
        print("""
    Trader Centre v0.1~DEC2_1
    -----------------------------------------------
    Combined Agent %(name)s Options
    t - Train Agent
    f - Train Agent Decision Model Only
    m - Interact with Market
    e - Explore
    b - Back
    -----------------------------------------------
        """ % {'name': name})

    command = input("Command #>")
    if command == "f":
        clear()
        tmtl = float(input("Training TTL: "))
        resett = float(input("Reset Time: "))
        print("\n")
        print("Training...")
        agent.train(ttl=tmtl, resetTime=resett)
        print("Training finished! Saving...")
        save.SaveTrader(dir, env, policy, name)
        initState=False
    elif command == "t":
        clear()
        tmtl = float(input("Regression Train TTL: "))
        tmttl = float(input("Decision Train TTL: "))
        bs = int(input("Regression Batch Size: "))
        resett = float(input("Reset Time: "))
        epoch = int(input("Epoch: "))
        print("\n")
        print("Training...")
        agent.adaptiveTrain(tmtl, tmttl, bs, resett, epoch)
        print("Training finished! Saving...")
        save.SaveTrader(dir, env, policy, name)
        initState=False
    elif command == "m":
        clear()
        rst = float(input("Reporting Time: "))
        ttl = float(input("TTL: "))
        startingFund = float(input("Starting Fund: "))
        startingBTC = float(input("Starting BTC: "))
        rBC = input("rBC (y/n): ")
        print("Creating Trading Agent...")
        nenv = MarketSimEnv(startingFund=startingFund, startingBTCAmount=startingBTC, rewardMultiplier=3, idleReward=-0.7, dataObject=None)
        nagent = CombinedAgent(policy, nenv)
        print("Agent Created! Starting Trades...")
        loopInitTime = time.time()
        startingTime = time.time()
        unacceptedTrades = 0
        trades = 0
        while True:
            loopTime = time.time()
            if loopTime-loopInitTime >= rst*60:
                print("There has been", trades, "trades and", unacceptedTrades, "unaccepted trades.")
                print("Currently, there is", nenv.fund, "USDT and ", nenv.coin, "BTC in the digital wallet.")
                print("The market data reports...", nenv.getLiveData(True)[0][-9:])
                loopInitTime = loopTime
            elif loopTime-startingTime >= ttl*60:
                print("There has been", trades, "trades and", unacceptedTrades, "unaccepted trades.")
                print("Currently, there is", nenv.fund, "USDT and ", nenv.coin, "BTC in the digital wallet.")
                print("The market data reports...", nenv.getLiveData(True)[0][-9:])
                print("Time! Shutting Down.")
                return
            if rBC == "y":
                action = nagent.produceAction(True)
            else:
                action = nagent.produceAction()
            if action[0] == 1:
                transaction = action[1].tolist()[0]
                price =  round(transaction[0], 2)
                coinAmount = round(transaction[1], 2)
                if transaction[2] > transaction[3]:
                    print("Buying...")
                    if nenv.fund >= price*coinAmount:
                        data = {
                            "price":price, 
                            "coinAmount":coinAmount, 
                            "orderBook":3,
                            "type": 0,
                        }
                        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
                        nenv.fund -= price*coinAmount
                        nenv.coin += coinAmount
                        print("Bidding order booked @ USDT",price,"for",coinAmount,"BTC.")
                        trades += 1
                    else:
                        unacceptedTrades += 1

                elif transaction[3] > transaction[2]:
                    print("Selling...")
                    if nenv.coin >= coinAmount:
                        data = {
                            "price":price, 
                            "coinAmount":coinAmount, 
                            "orderBook":3,
                            "type": 1,
                        }
                        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
                        nenv.coin -= coinAmount
                        nenv.fund += price*coinAmount
                        print("Asking order booked @ USDT",price," for",coinAmount,"BTC.")
                        trades += 1
                    else:
                        unacceptedTrades += 1
                
            else:
                unacceptedTrades += 1

    elif command == "e":
        clear()
        if initState:
            print("WARNING")
            print("The agent is currently in Initialization State!")
            print("It is important to train before exit, otherwise the agent will not be initialized.")
            print("Returning to master menu, please train.")
        else:
            print("""
        
        Agent %(name)s Information | COMBINATION
        -----------------------------------------------
        Starting Fund: $%(sf)s     |    Starting BTC: %(sbtc)s BTC
        Reward Multiplier: %(rm)s  |    Idle Reward: %(ir)s
        HL 1: %(hl1)s              |    HL 2: %(hl2)s
        HL 3: %(hl3)s              |    T Learning Rate: %(lr1)s
        LQ Learning Rate: %(lr2)s  |    Database Directory: %(dataDir)s
        -----------------------------------------------
        
            """ % {
                'name': name,
                'sf': env.initFund,
                'sbtc': env.initCoin,
                'rm': env.multiplier,
                'ir': env.idleReward,
                'lr1': policy.tLr,
                'lr2': policy.qLr,
                'hl1': policy.hiddenla1,
                'hl2': policy.hiddenla2,
                'hl3': policy.hiddenlb1,
                'dataDir': dir
            })
            input("Press ENTER to continue...")
    elif command == "b":
        clear()
        if initState:
            print("WARNING")
            print("The agent is currently in Initialization State!")
            print("It is important to train before exit, otherwise the agent will not be initialized.")
            print("Returning to master menu, please train.")
        else:
            main()
            return
    agentFC(name, agent, env, policy, dir, initState)

def agentEC(name, agent, env, policy, dir, initState=False):
    clear()
    if initState:
        print("""
    Trader Centre v0.1~DEC2_1
    -----------------------------------------------
    Bidder Agent %(name)s Options
    t - Train Agent
    This is the Initialization Menu.
    More options will be enabled after the agent is trained initially.
    -----------------------------------------------
        """ % {'name': name})
    else:
        print("""
    Trader Centre v0.1~DEC2_1
    -----------------------------------------------
    Bidder Agent %(name)s Options
    t - Train Agent
    e - Explore
    b - Back
    -----------------------------------------------
        """ % {'name': name})

    command = input("Command #>")
    if command == "t":
        clear()
        tmtl = float(input("Training TTL: "))
        print("\n")
        print("Training...")
        agent.train(ttl=tmtl)
        print("Training finished! Saving...")
        save.SaveTrader(dir, env, policy, name)
        initState=False
    elif command == "e":
        clear()
        if initState:
            print("WARNING")
            print("The agent is currently in Initialization State!")
            print("It is important to train before exit, otherwise the agent will not be initialized.")
            print("Returning to master menu, please train.")
        else:
            print("""
        
        Agent %(name)s Information | BIDDER
        -----------------------------------------------
        HL 1: %(hl1)s              |    HL 2: %(hl2)s
        Learning Rate: %(lr)s
        Database Directory: %(dataDir)s
        -----------------------------------------------
        
            """ % {
                'name': name,
                'hl1': str(policy.hiddenl1),
                'hl2': str(policy.hiddenl2),
                'lr': str(policy.learningRate),
                'dataDir': dir
            })
            input("Press ENTER to continue...")
    elif command == "b":
        clear()
        if initState:
            print("WARNING")
            print("The agent is currently in Initialization State!")
            print("It is important to train before exit, otherwise the agent will not be initialized.")
            print("Returning to master menu, please train.")
        else:
            main()
            return
    agentEC(name, agent, env, policy, dir, initState)


def agentIC(name, agent, env, policy, dir, initState=False):
    clear()

    if initState:
        print("""
    
    Trader Centre v0.1~DEC2_1
    -----------------------------------------------
    Buyer Agent %(name)s Options
    t - Train Agent
    This is the Initialization Menu.
    More options will be enabled after the agent is trained initially.
    -----------------------------------------------
    
    """ % {'name': name})
    else:
        initString = ""
        print("""
    
    Trader Centre v0.1~DEC2_1
    -----------------------------------------------
    Agent %(name)s Options
    t - Train Agent
    e - Explore
    b - Back
    -----------------------------------------------
    
    """ % {'name': name})

    command = input("Command #>")
    if command == "t":
        clear()
        tmtl = float(input("Training TTL: "))
        restt = float(input("Fund reset time: "))
        print("\n")
        print("Training...")
        agent.train(ttl=tmtl, resetTime=restt)
        print("Training finished! Saving...")
        save.SaveTrader(dir, env, policy, name)
        initState=False

    elif command == "e":
        clear()
        if initState:
            print("WARNING")
            print("The agent is currently in Initialization State!")
            print("It is important to train before exit, otherwise the agent will not be initialized.")
            print("Returning to master menu, please train.")
        else:
            print("""
        
        Agent %(name)s Information | BUYER
        -----------------------------------------------
        Starting Fund: $%(sf)s     |    Starting BTC: %(sbtc)s BTC
        Reward Multiplier: %(rm)s  |    Idle Reward: %(ir)s
        HL 1: %(hl1)s              |    Learning Rate: %(lr)s
        Database Directory: %(dataDir)s
        -----------------------------------------------
        
            """ % {
                'name': name,
                'sf': env.initFund,
                'sbtc': env.initCoin,
                'rm': env.multiplier,
                'ir': env.idleReward,
                'lr': policy.learningRate,
                'hl1': policy.hiddenLayer1Dim,
                'dataDir': dir
            })
            input("Press ENTER to continue...")

    elif command == "b":
        clear()
        if initState:
            print("WARNING")
            print("The agent is currently in Initialization State!")
            print("It is important to train before exit, otherwise the agent will not be initialized.")
            print("Returning to master menu, please train.")
        else:
            main()
            return

    agentIC(name, agent, env, policy, dir, initState)

def main():
    clear()
    print("""

    Trader Centre v0.1~DEC2_1
    -----------------------------------------------
    Options
    b - New Buyer Agent
    i - New Bidder Agent
    c - New Bidder+Buyer Agent
    o - Open Agent
    q - Quit
    -----------------------------------------------
    
    """)

    command = input("Command #>")
    clear()

    if command == "c":
        clear()
        print("\n")
        print("Agent Creation Tool")
        name = input("name (UUID will be generated in default): ")
        if name == "":
            name = str(uuid.uuid4())
        saveDir = input("Save directory: ").strip('\"')
        os.makedirs(os.path.join(saveDir, "data", name))
        print("------------------")
        print("Enviroment Section")
        print("------------------")
        sf = float(input("starting fund: "))
        sbtc = float(input("starting BTC amount: "))
        rmult = float(input("reward multiplier: "))
        irewd = float(input("idle reward: "))
        print("------------------")
        print("Policy Section")
        print("------------------")
        hl1d = int(input("regression hidden layer 1: "))
        hl2d = int(input("regression hidden layer 2: "))
        hl3d = int(input("decision hidden layer 1: "))
        gamma = float(input("discount factor: "))
        epilson = float(input("Base Epilson: "))
        epilsonDecay = float(input("Epilson Decay Step: "))
        print("------------------")
        print("LR Section")
        print("------------------")
        tlr = float(input("transaction learning rate: "))
        qlr = float(input("q-learning learning rate: "))
        logq = input("logging? (y/n): ")
        if "y" in logq:
            os.makedirs(os.path.join(saveDir, "tflogs", name))
            logq = os.path.join(saveDir, "tflogs", name)
        else:
            logq = "#NO#~"
        print("\n")
        print("Establishing Agent", name, "with given parameters...")
        env = PoloniexEnv(startingFund=sf, startingBTCAmount=sbtc, rewardMultiplier=rmult, idleReward=irewd)
        policy = CombinedAgentPolicy(hl1d, hl2d, hl3d, gamma, tlr, qlr, epilson, epilsonDecay, logq)
        agent = CombinedAgent(policy, env)
        print("Agent established. Saving...")
        save.SaveTrader(saveDir, env, policy, name)
        print("Done.")
        print("\n")
        print("Starting agent enviroment...")
        agentFC(name, agent, env, policy, saveDir, True)

    elif command == "i":
        clear()
        print("\n")
        print("Agent Creation Tool")
        name = input("name (UUID will be generated in default): ")
        if name == "":
            name = str(uuid.uuid4())
        saveDir = input("Save directory: ").strip('\"')
        os.makedirs(os.path.join(saveDir, "data", name))
        print("------------------")
        print("Policy Section")
        print("------------------")
        hl1d = int(input("hidden layer 1: "))
        hl2d = int(input("hidden layer 2: "))
        lr = float(input("learning rate: "))
        logq = input("logging? (y/n): ")
        if "y" in logq:
            os.makedirs(os.path.join(saveDir, "tflogs", name))
            logq = os.path.join(saveDir, "tflogs", name)
        else:
            logq = "#NO#~"
        print("\n")
        print("Establishing Agent", name, "with given parameters...")
        env = PoloniexEnv()
        policy = DeepRegressionPolicy(109, hl1d, hl2d, lr, logq)
        agent = BidderAgent(policy, env)
        print("Agent established. Saving...")
        save.SaveTrader(saveDir, env, policy, name)
        print("Done.")
        print("\n")
        print("Starting agent enviroment...")
        agentEC(name, agent, env, policy, saveDir, True)


    if command == "b":
        clear()
        print("\n")
        print("Agent Creation Tool")
        name = input("name (UUID will be generated in default): ")
        if name == "":
            name = str(uuid.uuid4())
        saveDir = input("Save directory: ").strip('\"')
        os.makedirs(os.path.join(saveDir, "data", name))
        print("Agent",name,"Parameters")
        print("------------------")
        print("Enviroment Section")
        print("------------------")
        sf = float(input("starting fund: "))
        sbtc = float(input("starting BTC amount: "))
        rmult = float(input("reward multiplier: "))
        irewd = float(input("idle reward: "))
        print("------------------")
        print("Policy Section")
        print("------------------")
        hl1d = int(input("hidden layer 1: "))
        lr = float(input("learning rate: "))
        logq = input("logging? (y/n): ")
        if "y" in logq:
            os.makedirs(os.path.join(saveDir, "tflogs", name))
            logq = os.path.join(saveDir, "tflogs", name)
        else:
            logq = "#NO#~"

        # env = PoloniexEnv(startingFund=100, startingBTCAmount=0.001, rewardMultiplier=2, idleReward=-0.00004)
        # policy = DeepQLearningPolicy(actions=list(range(2)), inputDim=103, hiddenLayer1Dim=51, hiddenLayer2Dim=25, learningRate=0.05)
        print("\n")
        print("Establishing Agent", name, "with given parameters...")
        env = PoloniexEnv(startingFund=sf, startingBTCAmount=sbtc, rewardMultiplier=rmult, idleReward=irewd)
        policy = DeepQLearningPolicy(actions=list(range(2)), inputDim=112, hiddenLayer1Dim=hl1d, learningRate=lr, logging=logq)
        agent = BuyerAgent(policy, env)
        print("Agent established. Saving...")
        save.SaveTrader(saveDir, env, policy, name)
        print("Done.")
        print("\n")
        print("Starting agent enviroment...")
        agentIC(name, agent, env, policy, saveDir, True)

    elif command == "o":
        clear()
        print("\n")
        path = input("Agent Database Path: ").strip('\"')
        id = input("Agent Name: ")
        atype = input("Agent Type (b/i/f): ")
        print("\n")
        print("Retriving agent...")
        agent, env, policy = save.OpenTrader(path, id, atype)
        print("Done.")
        print("\n")
        print("Starting agent enviroment...")
        if atype == "b":
            agentIC(id, agent, env, policy, path)
        elif atype == "i":
            agentEC(id, agent, env, policy, path)
        else:
            agentFC(id, agent, env, policy, path)

    elif command == "q":
        sys.exit()

    gc.collect()
    main()

if __name__=='__main__':
    main()
