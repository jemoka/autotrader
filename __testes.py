# import trader
# from trader.Policy import CombinedAgentPolicy
# from trader.Enviroment import PoloniexEnv
# from trader.Agent import CombinedAgent

# env = PoloniexEnv()
# pass

import requests, random, urllib, json
import numpy as np
from trader.commons import save
from trader.commons.encode import Autoencoder

def initializeDB():
    with urllib.request.urlopen('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC') as response:
        orderBook = json.load(response)

    asks = orderBook["asks"]
    bids = orderBook["bids"]

    for i in asks:
        price = float(i[0])
        amount = i[1]

        data = {
            "price":round(price, 2), 
            "coinAmount":round(amount, 10), 
            "orderBook":3,
            "type": 2,
        }

        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 

    for i in bids:
        price = float(i[0])
        amount = i[1]

        data = {
            "price":round(price, 2), 
            "coinAmount":round(amount, 10), 
            "orderBook":3,
            "type": 3,
        }

        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
  

initializeDB()

# data = {
#     "price":5000.0, 
#     "coinAmount":0.03, 
#     "orderBook":3,
#     "type": 1,
# }

# r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 



# encoder = Autoencoder(3, 1, learningRate=0.01)
# encoder.train(getOrderBook(), epoch=10000)

# # print(getOrderBook())
# # print(len(getOrderBook()))

# def getFullTicker():
#         with urllib.request.urlopen('http://127.0.0.1:8000/api/USDT_BTC/ticker?pk=3') as response:
#             tickerData = json.load(response)
#         return list(tickerData.values())


# def getLiveData():
#     ob = getOrderBook()
#     currentItem = ob[random.randint(0, 99)]
#     return (np.array(encoder.encode(ob)).flatten().tolist())+getFullTicker()

# import time, requests
# from trader.commons import save
# from trader.Agent import CombinedAgent
# from trader.Enviroment import PoloniexEnv
# from trader.Enviroment import MarketSimEnv
# from trader.Policy import CombinedAgentPolicy
# from multiprocessing import Process


# agent, env, policy = save.OpenTrader("C:\\Users\\kmliu_tcj2pqr\\Documents\\Developing Projects\\trader\\Agent Databases\\Group 3 - 2x PoC Combined Batch", "b3_c_02", "f")
# nenv = MarketSimEnv(startingFund=10000.0, startingBTCAmount=2.0, rewardMultiplier=3, idleReward=-0.7, dataObject=None)
# nagent = CombinedAgent(policy, env)

# startingTime = time.time()

# def runOrders():
#     loopInitTime = time.time()
#     action = nagent.produceAction()
#     if action[0] == 1:
#         print("Booking Order...")
#         transaction = action[1].tolist()[0]
#         price = transaction[0]
#         coinAmount = transaction[1]
#         if transaction[2] == 0:
#             data = {
#                 "price":price, 
#                 "coinAmount":coinAmount, 
#                 "orderBook":3,
#                 "type": 0,
#             }
#             r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
#             print("Bidding order booked @ USDT"+str(price)+" for "+str(coinAmount)+"BTC.")

#         elif transaction[2] == 1:
#             data = {
#                 "price":price, 
#                 "coinAmount":coinAmount, 
#                 "orderBook":3,
#                 "type": 1,
#             }
#             r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
#             print("Asking order booked @ USDT"+str(price)+" for "+str(coinAmount)+"BTC.")
